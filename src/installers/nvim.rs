use std::path::Path;

use super::installers::Installer;


pub struct Nvim;

const NVIM_PREFIX: &str = "/opt/nvim/nvim.appimage";

impl Installer for Nvim {
    fn install(&self) {
        todo!()
    }

    fn uninstall(&self) {
        todo!()
    }

    fn is_installed(&self) -> bool {
        Path::new(NVIM_PREFIX).exists()
    }
}
