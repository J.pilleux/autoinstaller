use crate::installers::installers::test_installer;
use clap::{Parser, Subcommand};

mod installers;

#[derive(Parser, Debug)]
struct Args {
    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    Check {
        #[clap(short, long, value_parser, value_name = "INSTALLER")]
        name: String,
    },
}

fn main() {
    println!("Hello, world!");
    test_installer();
}
