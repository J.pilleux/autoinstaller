pub fn test_installer() {
    println!("HelloFromInstaller");
}

pub trait Installer {
    fn install(&self);
    fn uninstall(&self);
    fn is_installed(&self) -> bool;
}
